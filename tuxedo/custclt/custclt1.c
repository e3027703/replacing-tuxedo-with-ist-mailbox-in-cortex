#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fml.h>
#include <cocrd.fd.h>


#include "atmi.h" /* TUXEDO */


#ifdef __STDC__
main(int argc, char *argv[])

#else

main(argc, argv)
int argc;
char *argv[];
#endif

{

int ret, cd, len = 0;
FBFR *sendfb, *recvfb;

/* Attach to BEA TUXEDO as a Client Process */
if (tpinit((TPINIT *) NULL) == -1) {
fprintf(stderr, "Tpinit failed\n");
exit(1);
}

printf( "Allocating memory\n");

if((sendfb = (char *)tpalloc("FML", NULL, 400))== NULL){
	fprintf(stderr,"Error allocating send buffer\n");
	tpterm();
	exit(1);
}

printf( "Adding PAN\n");
ret =Fadd(sendfb, C_PAN, "333837784626842", 0);


printf("RET = [%d].\n", ret);

//ret = sql_open(getenv("DBNAME"));

//printf("Sql_open [%d]\n", ret);

printf( "About to do tpcall\n");
ret = tpcall("CUSTSVC", sendfb, 0L, &sendfb, &len, 0);
printf( "Adding PAN\n");

printf("GET  reply done.\n");

if(ret == -1) {
fprintf(stderr, "Can't send request to service CUSTSVC\n");
tpterm();
exit(1);
}

printf ("Exiting\n");
tpterm();

}
