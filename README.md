**Replacing Tuxedo with IST/Mailbox in Cortex**

**1. Technologies and architecture used**

**What Tuxedo is doing in client/server architecture**

![Tuxedo Architectute](https://docs.oracle.com/cd/E35855_01/tuxedo/docs12c/int/images/servproc.gif)

- Middleware for msg-based transaction processing in client/server architecture
- Support multi-OS applications in one environment
- Provide APIs for interoperability problems through ATMI programming interface
- ATMI includes transaction managing function like request/response(tp_call), conversations, queuing(tp_enqueue), buffer management (FML32)
- Provide Bulletin board for services advertised, so client can send their msg to this queue and it will be distributed to respective service and responded.
- Security such as data protection and non-repudiation and authentication

**Challenges using Tuxedo:**
**Cost specific**
- 3rd party middleware tool cost and licensing, approX 70000 USD

**Resource utilization**
- Synchronous service calls :
	Every process needs to wait for response before processing next request, so queue will be high during peak hours led to multiple instance of services
    
**Other logical issues**
- Shared memory is not used for config and frequently table usage hence service restart is required for every changes
- More Addendum tables are used to store scheme and other datum led to heavy DB utilization while processing each transaction
- Reversing all the previous service changes if any one of the service failed while accessing multiple service during transaction processing

**2. What your code is designed for**
**How IST/Mailbox can replace Tuxedo:**
- IST/mailbox provides IPC mechanism for application message processing in various IST products for decades already
**Two main changes**
- Every Tuxedo ATMI API calls should be replaced with IST/Mailbox calls in all Cortex services
	eg. tp_call() to mb_write() ; TPSERVICEINFO() to mb_read(), service entry point etc.,
- Define global Fielded buffers like Tuxedo in IST and create routines to manipulate them. 
- Additionally, new controller to be developed in IST to restrict access to services/mailbox

**Other features in IST/Mailbox**
Distributed application server support through mbrns(remote name server)
Mailbox throttling
Monitoring and Controlling
Event Manager - handles timeout hence Asynchronous transaction processing can be done and use resources effectively
Task Manager - register and monitor task like services by Tuxedo
Rule Manager shared memory usage - Support Shared Memory feature to load frequently used information hence restart can be avoided
Mailbox priorities

**3. What your code was written in**
**Sample code used in POC**
A simple client server program in C has been created to send and receive message in Cortex using Tuxedo and same is modified to work using IST/Mailbox
Highlight's:
Replace service entry points in Tuxedo with mb_init calls and read message using mb_read()
Created similar Tuxedo routines to manipulate Fielded buffer like CF_add() CF_chg() in IST and used

**4. Open source or proprietary software used**

**5. Why it's cool**
Successful integration of in-house IST/Mailbox foundation in Cortex will save huge cost for Oracle Tuxedo license and open a way for accessing IST/Switch and Cortex business solution separately on-demand as well as single integrated robust card processing system. 
