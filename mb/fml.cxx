#include<stdlib.h>
#include <stdio.h>
#include<string.h>
#include "fml.h"
int initialized=1;
//tpalloc()
//void* tpalloc(int type, int subtype, int size)
void * tpalloc(char *type, char *subtype, long size)
{
	if(strcmp(type,"FML")==0)
	{
		FBFR *fb;
		if(size==0)
		size = 400;
		//fb.data=calloc(size, sizeof(char));
		fb=(FBFR*)calloc(size+8, sizeof(char));
		fb->data[0]='s';
		fb->used=0;
		fb->total_size=size;
		return fb;
	}
}

void* tprealloc(char *ptr, long size)
{
	FBFR *fb;
	fb=(FBFR*)ptr;
	//fb->data=realloc(ptr, size);
	fb->total_size=size;
	return ptr;
}

void tpfree(char* ptr)
{
	FBFR *fb;
	fb=(FBFR*)ptr;
	free((char*)fb->data);
}

int Fadd(FBFR **fbfr, FLDID fieldid, char *value, FLDLEN len)
{
	int copied=0;

	int fbtype;
	int fbsize;
	char tmp1[20];
	//FBFLD newfld;

	if(!initialized)
	{
		//init_fb();
	}
	else
	{
		get_fb_type_and_size(fieldid, &fbtype, &fbsize);
	}

	if(fbtype==FLD_STRING)
		fbsize=strlen(value);
	if((*fbfr)->total_size<(*fbfr)->used+fbsize)
		return -1;

	//Start searching for the field in fb
	{
	/*
	if(fbtype==FLD_STRING || fbtype==FLD_CARRAY)
	{
	*/
	//Add FLDID
	//newfld.fieldid=fieldid;
	memcpy(((*fbfr)->data+(*fbfr)->used), (char*)&fieldid, sizeof(fieldid));
	copied+=sizeof(fieldid);
	//Add LEN
	//newfld.fieldlen=fieldlen;
	memcpy((*fbfr)->data+(*fbfr)->used+copied, (char*)&fbsize, sizeof(size_t));
	copied+=sizeof(size_t);
	//Add VALUE
	memcpy((*fbfr)->data+(*fbfr)->used+copied, value, fbsize);
	copied+=fbsize;
	(*fbfr)->used+=copied;
	/*
	*      }
	else
	{
	//Add FLDID
	memcpy(fbfr->data+fbfr->used, fieldid, sizeof(fieldid));
	copied+=sizeof(fieldid);
	//Add LEN anyways. Makes it easy to parse
	//Add VALUE
	memcpy(fbfr->data+fbfr->used+copied, value, fbsize);
	copied+=strlen(value);
	fbfr->used+=copied;
	}
	*/
	}
	return 0;
}

//
int Fchg(FBFR **fb, FLDID fieldid, FLDOCC oc, char *value, FLDLEN len)
{
	int fbtype, fbsize;

	FBFLD *ptr;
	char *cur;
	int occ_cur=0;
	int count_cur=0;
	if(!initialized)
	{
		//init_fb();
	}
	get_fb_type_and_size(fieldid,&fbtype,&fbsize);
	if(fbtype==FLD_STRING)
	{
	}
	cur=(*fb)->data;
	while(cur!=NULL && *cur!=0)
	{
		if(memcmp(cur, (char*)fieldid, sizeof(fieldid))==0)
		{
			if(oc!=occ_cur)
			{
				occ_cur++;
				continue;
			}
			//found
			ptr=(FBFLD*)cur;
			memset(cur+8,0x0,(ptr->fieldlen));
			if(fbtype==FLD_STRING)
			{
				//May need to move elements
				if(ptr->fieldlen>strlen(value))
				{
					//New size is less than old size
					//Copy new data
					memcpy(cur+8, value, strlen(value));
					if(*(cur+8+ptr->fieldlen)!=0)
					{
						//Move rest of elements to left side
						memcpy(cur+8+strlen(value), cur+8+(ptr->fieldlen), ((*fb)->used-count_cur-8-ptr->fieldlen));
					}
					//Remove unnecessary trailing data
					memset((*fb)->data+(*fb)->used-(ptr->fieldlen-strlen(value)), 0x0, ptr->fieldlen-strlen(value));
					(*fb)->used-=(ptr->fieldlen-strlen(value));
				}
				else
				{
					//change it later
					char temp[1024]="";
					if(*(cur+8+ptr->fieldlen)!=0)
					{
						//Copy trailing data to temp buf
						memcpy(temp,cur+8+ptr->fieldlen, (*fb)->used-count_cur-8-ptr->fieldlen);
						//Write new data
						memcpy(cur+8, value, strlen(value));
						//Copy trailing data back
						memcpy(cur+8+strlen(value), temp, (*fb)->used-count_cur-8-ptr->fieldlen);
					}
					else
					{
						//No trailing data. So copy.
						memcpy(cur+8, value, strlen(value));
					}
					(*fb)->used+=(strlen(value)-ptr->fieldlen);
				}
				ptr->fieldlen=strlen(value);
			}
			else
			{
				memcpy(cur+8, value,fbsize);
			}
			return 0;
		}
		else
		{
			ptr=(FBFLD*)cur;
			cur+=(ptr->fieldlen);
			count_cur+=ptr->fieldlen;
			cur+=2*sizeof(int);
			count_cur+=2*sizeof(int);
		}
	}
	return -1;
}


int Fdel(FBFR *fb, FLDID fieldid, FLDOCC oc)
{
	int fbtype, fbsize;
	FBFLD *ptr;
	char *cur;
	int occ_cur=0;
	int count_cur=0;
	if(!initialized)
	{
		//init_fb();
	}
	cur=fb->data;
	while(cur!=NULL && *cur!=0)
	{
		if(memcmp(cur, (char*)&fieldid, sizeof(fieldid))==0)
		{
			int templen;
			if(oc!=occ_cur)
			{
				occ_cur++;
				continue;
			}
			//Erase data
			memset(cur+8,0x0,(ptr->fieldlen));
			if(*(cur+8+(ptr->fieldlen))!=0)
			{
				//Move trailing data to left
				templen=ptr->fieldlen;
				memcpy(cur, cur+8+(ptr->fieldlen), (fb->used-count_cur-8-ptr->fieldlen));
				//Erase the trailing unnecessary duplicates
				memset(fb->data+(fb->used-8-templen), 0x0, templen+8);
			}
			fb->used-=templen;
			return 0;
		}
		else
		{
			ptr=(FBFLD*)cur;
			cur+=(ptr->fieldlen);
			count_cur+=ptr->fieldlen;
			cur+=2*sizeof(int);
			count_cur+=2*sizeof(int);
		}
	}
	return -1;
}

int fbdump(FBFR** fb)
{
int size=0;
int i=0,j=0;
int total=0;
char c;

printf( "Dumping\n");

size=(*fb)->used;
for(i=0;i<size;i+=32)
{
for(j=0;j<32 && j+i<size;j++)
{
c=(*fb)->data[j+i];
if(c>31 && c<127)
fprintf(stdout, "%c  ", c);
else
fprintf(stdout, "?  ");
}
fprintf(stdout,"\n");
for(j=0;j<32&& j+i<size;j++)
{
fprintf(stdout, "%02x ", (*fb)->data[i+j]);
}
fprintf(stdout,"\n");
} 

}

int get_fb_type_and_size(int fieldid, int *fbtype, int *fbsize)
{
	if(fieldid==C_PAN||fieldid==C_CARDID)
	{
		*fbtype=FLD_STRING;
		*fbsize=0;
	}
	else
	{
		*fbtype=1;//number for int
		*fbsize=4;//sizeof int
	}
	return 0;
}


