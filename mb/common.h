#ifndef __COMMON_H__
#define __COMMON_H__

#include "fml.h"

#define SUCCEED         0
#define FAIL            -1
#define GLOBAL_SHM_COUNTKEY     9998
#define GLOBAL_SHM_QKEY 9999
#define OUTFILE         stdout
#define MAX_SHM_MQ_ENTRY        10
#define GLOBAL_TYPE     989


typedef struct
{
	char service_name[33];
	int mqid;
}MQROW_t;

typedef struct
{
	char    name[32];
	long    flags;
	char    *data;
	long    len;
	int     cd;
	long    appkey;
	long    cltid;
}TPSVCINFO;

typedef struct
{
	char service_name[33];
	void *function_ptr(FBFR*);
}SERVICE_MAP;

typedef struct
{
	long    type;
	TPSVCINFO       qdata;
}MQMSG;


#endif
