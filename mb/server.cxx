#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <mb.h>
#include <shc.h>

#include "fml.h"


#define CUSTSVCNAME "CUSTOMSVC"

int ret;
FILE *fd;
FBFR *G_p_fb;
void CUSTOMSVC(FBFR *rqst);

SERVICE_MAP     svcmap[]={
	{"CUSTOMSVC",&CUSTOMSVC},
	{"CUSTOMSVC",&CUSTOMSVC}
	};
int     NUMBEROFSERVICES;
int     mbid;

void CUSTOMSVC(FBFR *rqst)
{
	char fbdata[10];
	char cur[]="7876567";
	FBFR outbuf;

	fd = stdout;

	fprintf(fd, "Service Entry\n");

	//Get FB
	G_p_fb = rqst;
	fprintf(fd, "Received:\n");
	fbdump(&G_p_fb);

	ret = Fadd(&G_p_fb, C_CARDID, cur, 0);
	fprintf(fd, " ADDED. RET - [%d] , C_CARDID - [%s] \n", ret, cur);

	fprintf(fd, "CUSTOMSVC service called with:");
	fbdump(&G_p_fb);
	fprintf(fd, "Got new message\n");

	outbuf=*G_p_fb;
	//ntp_return(TPSUCCESS, tpu_data, (char *)rqst->data, 50, 0L);

	mb_write(mbLastSenderId(), mbid, &outbuf, 400);
	//mb_write(mbLastSenderId(), mbid, G_p_fb, 400);
}

//Auto gen below code

int main(int argc, char **argv)

{
	/*int qshmid;
	int qcount;
	int count_shmid;*/
	int ret, i;
	int mbid;
	char service_name[32];
	FBFR inmsg;

	//Read service key and service name from CLA
	if(argc != 2)
	{
		fprintf(fd, "Not allowed to run\n");
		exit(-1);
	}
	strcpy(service_name, argv[1]);

	mb_init();
	mbid=mb_createmailbox(service_name, getpid(), 0);
	if(mbid < 0) 
	{
		printf("Cant create the client mailbox\n");
		exit(0);
	}
	for(i=0; i<NUMBEROFSERVICES; i++)
	{
		if(strcmp(svcmap[i].service_name, service_name)==0)
		break;
	}

	//Loop read function on message queue
	while(1)
	{
		//mb_read(mbid, &inmsg, sizeof(inmsg));
		mb_read(mbid, &inmsg, 400);

		//Call your Service
		//*(svcmap[i].function_ptr)(&inmsg);
		svcmap[i].function_ptr(&inmsg);
	}
	mb_disconnect();

	//Exit
	return 0;
}
