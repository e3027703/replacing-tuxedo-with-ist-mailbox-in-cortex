#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mb.h>
#include <shc.h>

#include "fml.h"

#ifdef __STDC__
main(int argc, char *argv[])

#else

main(argc, argv)
int argc;
char *argv[];
#endif

{

	int ret, cd, len = 0;
	FBFR *sendfb, *recvfb;
	int client_id;
	int sender_id, server_id;


	mb_init();
	client_id=mb_createmailbox("CUSTOMCLIENT", getpid(), 0);

	printf( "Allocating memory\n");

	if((sendfb = (FBFR*)tpalloc("FML", NULL, 400))== NULL)
	{
		fprintf(stderr,"Error allocating send buffer\n");
		exit(1);
	}
	if((recvfb = (FBFR*)tpalloc("FML", NULL, 400))== NULL)
	{
		fprintf(stderr,"Error allocating recv buffer\n");
		exit(1);
	}

	printf( "Adding PAN\n");
	ret =Fadd(&sendfb, C_PAN, "333837784626842", 0);


	printf("RET = [%d].\n", ret);

	printf( "About to do mb_write\n");
	server_id=mb_locatemailbox("CUSTOMSVC");
	printf("Located service mb[%d]\n", server_id);

	mb_write(server_id, client_id, sendfb, 400);
	//mb_write(server_id, client_id, sendfb, sizeof(*sendfb));
	printf("Message written\n");
	mb_read(client_id, recvfb, 400);
	printf("GET  reply done.\n");
	fbdump(&recvfb);

	printf ("Exiting\n");
	//mb_deletemailbox(client_id);
	return 0;

}
