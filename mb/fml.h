#ifndef __FML_H__
#define __FML_H__


#define FLDID   int
#define FLDLEN int
#define FLDOCC int
#define FLD_STRING 4

#define C_PAN 1001
#define C_SEQNO 1002
#define C_CARDID 1003

typedef struct
{
	int used;
	int total_size;
	char data[400];
}FBFR;

typedef struct
{
	int fieldid;
	int fieldlen;
}FBFLD;

typedef struct
{
        char service_name[33];
	        void (*function_ptr)(FBFR*);
		}SERVICE_MAP;

void * tpalloc(char *type, char *subtype, long size);
void* tprealloc(char *ptr, long size);
void tpfree(char* ptr);
int Fadd(FBFR **fbfr, FLDID fieldid, char *value, FLDLEN len);
int Fchg(FBFR **fb, FLDID fieldid, FLDOCC oc, char *value, FLDLEN len);
int Fdel(FBFR *fb, FLDID fieldid, FLDOCC oc);
int fbdump(FBFR** fb);

int get_fb_type_and_size(int fieldid, int *fbtype, int *fbsize);

#endif
